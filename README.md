# Slack Workflows

This project houses the source of the workflows that GitLab Support creates using the [Slack Workflow Builder](https://slack.com/help/articles/360035692513-Guide-to-Workflow-Builder).